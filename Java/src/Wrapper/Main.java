package Wrapper;


public class Main {
	
	private Float f;
	
	public Main(){
		if(f == null){
			f = (float) 0.0;
		}
	}

	public Float getF() {
		return f;
	}

	public void setF(Float f) {
		this.f = f;
	}




	public static void main(String[] args) {
		
		Main m = new Main();
		System.out.println(m.getF());
		m.setF((float) 4.5);
		System.out.println(m.getF());
		
		System.out.println(m.getF().floatValue());
	}

}
