package Constructores;

public class ConstructorSobrecargado {
	
	String nombre;
	
	
	public ConstructorSobrecargado(){}
	
	public ConstructorSobrecargado(String nombre){
		this.nombre = nombre;
	}


	public static void main(String[] args) {
		
		ConstructorSobrecargado c1 = new ConstructorSobrecargado();
		ConstructorSobrecargado c2 = new ConstructorSobrecargado("Alex");
	}

}
