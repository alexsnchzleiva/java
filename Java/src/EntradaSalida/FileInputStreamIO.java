package EntradaSalida;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


public class FileInputStreamIO {

	
	@SuppressWarnings("resource")
	public static void main(String [] args) throws IOException{
		
		String fichero = "Ejemplo1.txt";
		
		File file = new File(fichero);	
		FileInputStream fis = new FileInputStream(file);
		
		byte [] bt = new byte[(int)file.length()]; 
		fis.read( bt );
		String cadena = new String( bt );

		System.out.println(cadena);
	
	}
}