package EntradaSalida;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class BufferedReaderIO {


	public static void main(String[] args) throws IOException {

		File file = new File("texto.txt");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		
		int i;		
		while(	(i = br.read()) != -1){
			System.out.print((char)i);
		}
		
		if(br != null){
			br.close();
		}
		
	}

}
