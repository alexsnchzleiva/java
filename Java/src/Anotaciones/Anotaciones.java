package Anotaciones;


public class Anotaciones {
	
	@Deprecated
	public static void deprecado(){
		System.out.println("Método deprecated");
	}

	@Override
	public String toString(){
		return"";
	}
	
	@SuppressWarnings("fallthrough")
	public static void suprimirWarnings(){
		System.out.println("Método SuperWarning");
		
		int a = 3;
		
		switch(a){
			case 1: System.out.println(a);
			case 2: System.out.println(a);
			case 3: System.out.println(a);
			case 4: System.out.println(a);
		}
	}
	
	

	public static void main(String[] args) {

		deprecado();
		suprimirWarnings();
	}

}
