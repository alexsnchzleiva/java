package Cast;

import java.util.ArrayList;
import java.util.List;

public class ConversionImplicita {
	
	public static void main(String args[]){
		
		List <Integer> al = new ArrayList <Integer> ();
		al.add(new Integer(1));
		al.add(new Integer(2));
		al.add(new Integer(3));
		al.add(new Integer(4));
		al.add(new Integer(5));
		al.add(new Integer(6));
		al.add(new Integer(7));
		al.add(new Integer(8));
		al.add(new Integer(9));
		
		for(Integer i:al){
			System.out.println(i);
		}
		
	}

}
