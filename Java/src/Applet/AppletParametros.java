
package Applet;

import java.applet.Applet;
import java.awt.Graphics;


public class AppletParametros extends Applet {
    
    String nombre;


    @Override
    public void init() {
        nombre = getParameter("NOMBRE");
    }
    
    @Override
    public void start(){}
    
    @Override
    public void stop(){}
    
    @Override
    public void destroy(){}
    
    @Override
    public void paint(Graphics g){
        g.drawString("Hola Mundo " + nombre, 70, 70);
    }
    
}
