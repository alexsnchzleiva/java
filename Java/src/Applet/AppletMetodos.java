
package Applet;

import java.applet.Applet;
import java.awt.Graphics;

public class AppletMetodos extends Applet {
    
    String nombre;
    
    @Override
    public void init() {
        resize(1000,500);
        nombre = "Alejandro";
        System.out.println("Init");
    }
    
    @Override
    public void start(){
        System.out.println("Start");
    }
    
    @Override
    public void stop(){
        System.out.println("Stop");
    }
    
    @Override
    public void destroy(){
        System.out.println("Destroy");
    }
    
    @Override
    public void paint(Graphics g){
        g.drawString("Bienvenidos a un Applet", 30, 30);
        System.out.println("Paint");
    }
    
    @Override
    public void update(Graphics g){
        System.out.println("Update");
    }
    
    @Override
    public void repaint(){
        System.out.println("Repaint");
    }
    
    @Override
    public void print(Graphics g){
        System.out.println("Print");
    }
    
}
