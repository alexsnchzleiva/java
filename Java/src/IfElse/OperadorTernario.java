
package IfElse;


public class OperadorTernario {

	
    public static void main(String[] args) {
        
        int a = 0;
        int b = 1;
        
        int c = (a==0) ? 1 : 2;
        
        System.out.println(c);
    }
}
