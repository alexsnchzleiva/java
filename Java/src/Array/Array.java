package Array;


public class Array {


	public static void main(String[] args) {
		
		String [] array1;
		array1 = new String[5];
		
		String [] dias = {"Lunes","Martes","Miércoles","Jueves","Viernes"};
		
		int [][] array;
		
		array = new int[3][];
		
		array[0] = new int[2];
		array[1] = new int[3];
		array[2] = new int[4];
		
		for(int i=0; i<array.length; i++){
			for(int j=0; j<array[i].length; j++){
				array[i][j] = i+j;
			}
		}
		
		for(int i=0; i<array.length; i++){
			System.out.println();
			for(int j=0; j<array[i].length; j++){
				System.out.print(array[i][j]);
			}
		}
		

	}

}
