package Array;

import java.util.Arrays;


public class ArraySortOrden {


	public static void main(String[] args) {
		
		String [] a = new String[5];
		a[0] = "Jorge";
		a[1] = "Pedro";
		a[2] = "Alejandro";
		a[3] = "Zoe";
		a[4] = "Club";
		
		Arrays.sort(a);
		
		for(String valor:a){
			System.out.println(valor);
		}
		
	}

}
