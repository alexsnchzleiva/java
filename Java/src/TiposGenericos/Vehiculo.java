
package TiposGenericos;


public abstract class Vehiculo {
	
    abstract public void arrancar();
    abstract public void detener();
}
