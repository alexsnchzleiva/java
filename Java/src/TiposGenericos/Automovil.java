
package TiposGenericos;



public class Automovil extends Vehiculo {

    @Override
    public void arrancar() {
        System.out.println("El coche arranca...");
    }

    @Override
    public void detener() {
        System.out.println("El coche se detiene...");
    }
    
}
