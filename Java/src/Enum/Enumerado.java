
package Enum;


public class Enumerado {
    
    static public enum Estacion{
        Primavera,Verano,Otoño,Invierno
    };
    
    public static void main(String args[]){
        
        for(Estacion e:Estacion.values()){
            System.out.println("La estación es " + e + " y es la " + (e.ordinal() + 1));
        }           
    }
    
}
