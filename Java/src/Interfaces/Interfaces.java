package Interfaces;

public class Interfaces implements Runnable{

	public static int version = 0;
	
	@Override
	public void run() {
		System.err.println("Hola " + version++);
	}
	
	

	public static void main(String[] args) {
		
		Interfaces interfaces1 = new Interfaces();
		Interfaces interfaces2 = new Interfaces();
		Interfaces interfaces3 = new Interfaces();
		Interfaces interfaces4 = new Interfaces();
		
		Thread th1 = new Thread(interfaces1);
		Thread th2 = new Thread(interfaces2);
		Thread th3 = new Thread(interfaces3);
		Thread th4 = new Thread(interfaces4);
		
		th1.start();
		th2.start();
		th3.start();
		th4.start();
	}

}
