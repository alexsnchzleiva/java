
package Metodos;


public class Metodos {
    
    String nombre;
    static int numero = 0;

    public Metodos(String nombre) {
        this.nombre = nombre;
        ++this.numero;
    }
    
    public static void verVersion(){
        System.out.println(numero);
    }
    
    //public abstract void verAbstracto();
    
    public native void verNative();
    
    public synchronized void verHilo(){
    	
    }
    
    
    
    public static void main(String[] args) {
        
        Metodos.verVersion();
        Metodos m = new Metodos("Alejandro");
        Metodos.verVersion();
        
    }
}
