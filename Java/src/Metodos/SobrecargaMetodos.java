
package Metodos;


public class SobrecargaMetodos {
    
    public void verNombre(){
        System.out.println("-");
    }
    
    public void verNombre(String a){
        System.out.println(a);
    }
    
    public void verNombre(String a,String b){
        System.out.println(a+b);
    }
    
    public void verNombre(String a,int b){
        System.out.println(a+b);
    }


    
    
    public static void main(String[] args) {
        SobrecargaMetodos sm = new SobrecargaMetodos();
        sm.verNombre();
        sm.verNombre("Alex");
        sm.verNombre("Alex", "Sánchez");
        sm.verNombre("Alex", 3);
    }
}
