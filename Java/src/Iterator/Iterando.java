package Iterator;

import java.awt.Label;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


public class Iterando {
	
	static void muestraElementos(Object obj){
		
		if(obj instanceof Map){
			obj = ((Map<?, ?>)obj).entrySet();
		}
		
		if(obj instanceof Collection){
			Collection<?> coleccion = (Collection<?>)obj;
			Iterator<?> it = coleccion.iterator();	
			
			while(it.hasNext()){
				System.out.println(it.next());
			}
		}
		else
			System.out.println("Esto no es una colección");
	}


	
	public static void main(String[] args) {
		
		List <String> lista = new ArrayList<String>();
		lista.add("Línea 1");
		lista.add("Línea 2");
		muestraElementos(lista);
		
		Set<String>conjunto = new TreeSet<String>();
		conjunto.add("Línea 3");
		conjunto.add("Línea 4");
		muestraElementos(conjunto);
		
		Map <String,String> mapa = new HashMap <String,String>();
		mapa.put("Línea 5: Clave", "Línea 5: Valor");
		mapa.put("Línea 6: Clave", "Línea 6: Valor");
		muestraElementos(mapa);
		
		Label label = new Label("Label");
		muestraElementos(label);
		
	}

}
