package Threads;


public class HiloRunnable implements Runnable{
	
	Thread t;
	String nombre;
	
	public HiloRunnable(String nombre){
		super();
		this.nombre = nombre;
	}
	
	@Override
	public void run() {
		t = new Thread();
		
		for(int i=0;i<100;i++){
			Thread.yield();
			System.out.println(this.nombre + ": " + i);	
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	

	public static void main(String[] args) {
		Thread t1 = new Thread(new HiloRunnable("Hilo 1"));
		Thread t2 = new Thread(new HiloRunnable("Hilo 2"));
		Thread t3 = new Thread(new HiloRunnable("Hilo 3"));
		Thread t4 = new Thread(new HiloRunnable("Hilo 4"));
		Thread t5 = new Thread(new HiloRunnable("Hilo 5"));
		Thread t6 = new Thread(new HiloRunnable("Hilo 6"));
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();
	}

}
