package Threads;


public class ThreadHolaMundo extends Thread{
	
	private String nombre;
	private int retardo;
	

	public ThreadHolaMundo(String nombre, int retardo) {
		super();
		this.nombre = nombre;
		this.retardo = retardo;
	}
	
	@Override
	public void run(){
		try {
			super.sleep(retardo);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Hola Mundo " + this.nombre + " " +  this.retardo);
	}

	public static void main(String[] args) {
		
		ThreadHolaMundo t1,t2,t3;
		t1 = new ThreadHolaMundo("Thread 1",(int)(Math.random()*2000));
		t2 = new ThreadHolaMundo("Thread 2",(int)(Math.random()*2000));
		t3 = new ThreadHolaMundo("Thread 3",(int)(Math.random()*2000));
		
		t1.setPriority(1);
		t2.setPriority(5);
		t3.setPriority(10);
		
		t1.start();
		t2.start();
		t3.start();

	}

}
