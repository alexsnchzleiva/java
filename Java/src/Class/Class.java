package Class;


public class Class {
	
	public void casa(){}
	public void casa2(){}


	public static void main(String[] args) {

		Class c = new Class();
		
		System.out.println(c.getClass());
		System.out.println(c.getClass().getCanonicalName());
		System.out.println(c.getClass().getModifiers());
		System.out.println(c.getClass().getName());
		System.out.println(c.getClass().getSimpleName());
		System.out.println(c.getClass().hashCode());
		System.out.println(c.getClass().getMethods());
		System.out.println(c.getClass().getSuperclass());
		
		try {
			Class r = c.getClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
	}

}
