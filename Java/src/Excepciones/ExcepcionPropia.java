package Excepciones;


@SuppressWarnings("serial")
public class ExcepcionPropia extends Exception{
	
	public String getMessage(){
		return "Exception";
	}
	
	
	public static void main(String [] args){
		
		try{
			throw (new ExcepcionPropia());
		}
		catch(ExcepcionPropia l){
			System.out.println(l.getMessage());
			l.printStackTrace();
		}
		
	}

}
