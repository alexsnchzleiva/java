package System;

import java.io.IOException;


public class RuntimeSystem {
	
	
	public static void main(String [] args){
		
		Runtime r = Runtime.getRuntime();
		Process p = null;
		String [] command = {"/Applications/TextEdit.app/Contents/MacOS/TextEdit"};
		
		System.out.println("Total memoria: " + r.totalMemory());
		System.out.println("Memoria libre: " + r.freeMemory());
		
		try {
			p = r.exec(command);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
