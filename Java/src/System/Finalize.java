package System;


public class Finalize {
	
	String nombre;
	
	public Finalize(String text){
		nombre = text;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	
	public static void main(String[] args) {
		
		Finalize f = new Finalize("Alex");
		System.out.println(f.getNombre()); 
		System.runFinalization();
	}

}
