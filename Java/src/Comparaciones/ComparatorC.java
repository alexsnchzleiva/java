package Comparaciones;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ComparatorC implements Comparator<String> {

	
	@Override
	public int compare(String o1, String o2) {
		String s1 = o1.toLowerCase();
		String s2 = o2.toLowerCase();
		
		return s1.compareTo(s2);
	}
	
	
	public static void main(String[] args) {

		List <String> hs = new ArrayList<String>();
		hs.add("Casa");
		hs.add("Perro");
		hs.add("Gato");
		hs.add("gato");
		hs.add("Gato");
		hs.add("Norma");
		hs.add("alex");
		hs.add("Alex");
		hs.add("asco");
		hs.add("guapa");
		hs.add("cero");
		
		ComparatorC c = new ComparatorC();
		
		Collections.sort(hs, c);
		
		for(String valor:hs){
			System.out.println(valor);
		}

	}

}
