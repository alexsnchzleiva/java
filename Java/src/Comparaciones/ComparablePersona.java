package Comparaciones;

import java.util.Set;
import java.util.TreeSet;


public class ComparablePersona implements Comparable <ComparablePersona> {
	
	String nombre;
	String apellidos;
	Integer edad;

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	
	@Override
	public int compareTo(ComparablePersona o) {
		return o.getEdad().compareTo(this.getEdad());
	}



	public static void main(String[] args) {
		
		ComparablePersona p1 = new ComparablePersona();
		p1.setNombre("Alejandro");
		p1.setApellidos("Sánchez");
		p1.setEdad(31);
		ComparablePersona p2 = new ComparablePersona();
		p2.setNombre("Jorge");
		p2.setApellidos("Antón");
		p2.setEdad(28);
		ComparablePersona p3 = new ComparablePersona();
		p3.setNombre("Ivan");
		p3.setApellidos("Ramón");
		p3.setEdad(22);
		ComparablePersona p4 = new ComparablePersona();
		p4.setNombre("Berto");
		p4.setApellidos("Millán");
		p4.setEdad(24);
		ComparablePersona p5 = new ComparablePersona();
		p5.setNombre("Alvaro");
		p5.setApellidos("pombo");
		p5.setEdad(45);
		
		Set <ComparablePersona> l = new TreeSet<ComparablePersona>();
		l.add(p1);
		l.add(p2);
		l.add(p3);
		l.add(p4);
		l.add(p5);
		
		
		for(ComparablePersona p:l){
			System.out.print(p.getNombre() + " - ");
			System.out.println(p.getEdad());
		}

	}


}
