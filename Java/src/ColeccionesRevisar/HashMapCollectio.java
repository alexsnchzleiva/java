package ColeccionesRevisar;

import java.util.HashMap;


public class HashMapCollectio {


	public static void main(String[] args) {
		
		HashMap <String,String> hm = new HashMap <String,String>();
		hm.put("coche", "car");
		hm.put("salto", "jump");
		hm.put("gato", "cat");
		hm.put("andar", "walk");
		hm.put("verdad", "true");
	
		System.out.println(hm.get("coche"));
		System.out.println(hm.get("verdad"));	
	}

}
