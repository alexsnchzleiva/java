package ColeccionesRevisar;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;


public class TreeMapCollection {

	
	public static void main(String[] args) {
		TreeMap <String,String> tm = new TreeMap<String,String>();
		tm.put("hola", "hello");
		tm.put("gato", "cat");
		tm.put("perro", "dog");
		tm.put("coche", "car");

		
		Set s = tm.entrySet();
		Iterator it = s.iterator();
		
		while(it.hasNext()){
			System.out.println(it.next());
		}
		
	}
}
