package ColeccionesRevisar;

import java.util.PriorityQueue;


public class PriorityQueueCollection {
	
	public static void main(String[] args) {
		
		PriorityQueue<String> pq = new PriorityQueue<String>();
		pq.add("Jorge");
		pq.add("Alex");
		pq.add("Ramón");
		
		pq.poll();
		
		System.out.println(pq.toString());
	}

}
