package ColeccionesRevisar;

import java.util.ArrayList;
import java.util.ListIterator;


public class ArraListCollection {

	
	public static void main(String[] args) {

		ArrayList <String> al = new ArrayList<String>();
		
		al.add("Primero");
		al.add("Segundo");
		al.add("Tercero");
		al.add(0,"Cero");
		al.add(4,"Cuarto");
		
		if(al.contains("Primero")){
			System.out.println("Existe un primero");
		}
		else{
			System.out.println("No existe");
		}
		
		ListIterator <String> li = al.listIterator();
		
		while(li.hasNext()){
			System.out.println(li.next());
		}
	
	}

}
