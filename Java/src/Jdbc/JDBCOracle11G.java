
package Jdbc;

import java.sql.*;


public class JDBCOracle11G {

 
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        
        Connection conexion;
        Statement sentencia;
        ResultSet resultado;
        System.out.println("Iniciando programa");
        
        Class.forName("oracle.jdbc.OracleDriver").newInstance();
        conexion = DriverManager.getConnection("jdbc:oracle:thin:localhost:1521","ALEX","inlandempire");
        sentencia = conexion.createStatement();
        sentencia.execute("DROP TABLE JDBC");
        sentencia.execute("CREATE TABLE JDBC (NOMBRE VARCHAR(20))");
        sentencia.execute("INSERT INTO JDBC NOMBRE VALUES('ALEJANDRO')");
        sentencia.execute("INSERT INTO JDBC NOMBRE VALUES('JORGE')");
        sentencia.execute("INSERT INTO JDBC NOMBRE VALUES('IVAN')");
        
        resultado = sentencia.executeQuery("SELECT NOMBRE FROM JDBC");
        
        while(resultado.next()){
            String nombre = resultado.getString("NOMBRE");
            System.out.println(nombre);
        }
        
        System.out.println("Ejecución finalizada." + " AutoCommit: " + conexion.getAutoCommit());
        
        DatabaseMetaDataClase dbmdc = new DatabaseMetaDataClase(conexion);
        System.out.println("\nMostramos el nombre de las tablas:");
        dbmdc.mostrarTablas();
        
    }
}
