package String;

public class StringBufferSTR {

	
	public static void main(String[] args) {
		
		String cadena = "Ejemplo cadena";
		
		StringBuffer sb = new StringBuffer(cadena.length());
		System.out.println(sb.capacity());
		
		sb.append(cadena);
		System.out.println(sb);
		
		sb.append(" hola");
		System.out.println(sb);
		
		sb.insert(8, "Adios ");
		System.out.println(sb);

	}

}
