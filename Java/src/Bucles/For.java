package Bucles;


public class For {


	public static void main(String[] args) {
		
		String [] arrayCadenas = new String [5];
		arrayCadenas[0] = "Alejandro";
		arrayCadenas[1] = "Juán";
		arrayCadenas[2] = "Pedro";
		arrayCadenas[3] = "Pere";
		arrayCadenas[4] = "Chacho";
		
		for(int i=0;i<arrayCadenas.length;i++){
			System.out.println(arrayCadenas[i]);
		}
		
		for(String valor:arrayCadenas){
			System.out.println(valor);
		}
		
	}
	
	

}
