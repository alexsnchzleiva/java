package Bucles;

public class While {


	public static void main(String[] args) {
		
		boolean comprobacion = true;
		
		while(comprobacion){
			System.out.println("Bucle");
			comprobacion = false;
		}
		
		do{
			System.out.println("Ejemplo Do While");
		}
		while(comprobacion);

	}

}
