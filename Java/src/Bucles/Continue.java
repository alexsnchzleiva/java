
package Bucles;


public class Continue {


    public static void main(String[] args) {
        
        for(int i=0;i<=10;i++){
            System.out.println("\n" + i);
            for(int j=65;j<75;j++){
                System.out.print((char)j);
                if(j==68)
                    continue;
            }
            System.out.println("");
        }
        
        System.out.println("-------------------------------------");
        
        etiqueta1 : for(int i=0;i<=10;i++){
            System.out.println("\n" + i);
            etiqueta2 : for(int j=65;j<75;j++){
                System.out.print((char)j);
                if(j==68)
                    continue etiqueta1;
            }
            System.out.println("");
        }
        
    }
}
